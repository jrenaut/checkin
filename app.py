from flask import Flask, g, render_template, request, session, redirect, url_for
from pymongo import MongoClient
from bson.objectid import ObjectId
from bson.son import SON
import os
from pprint import pformat
from cloudinary import config as cloudinary_config
from cloudinary.uploader import upload
from cloudinary.utils import cloudinary_url
from datetime import datetime, timedelta
import boto3
from botocore.exceptions import ClientError
from botocore.config import Config

from utils import hash_password, check_password
from filters import format_datetime, login_required

from logging.config import dictConfig

dictConfig(
    {
        "version": 1,
        "formatters": {
            "default": {
                "format": "[%(asctime)s] %(levelname)s in %(module)s: %(message)s",
            }
        },
        "handlers": {
            "wsgi": {
                "class": "logging.StreamHandler",
                "stream": "ext://flask.logging.wsgi_errors_stream",
                "formatter": "default",
            }
        },
        "root": {"level": "INFO", "handlers": ["wsgi"]},
    }
)


my_config = Config(
    region_name="us-east-1",
    signature_version="v4",
    retries={"max_attempts": 10, "mode": "standard"},
)

app = Flask(__name__)
app.secret_key = os.environ["SECRET_KEY"]
app.jinja_env.filters["datetime"] = format_datetime
cloudinary_config(
    cloud_name=os.environ["CLOUDINARY_CLOUD_NAME"],
    api_key=os.environ["CLOUDINARY_API_KEY"],
    api_secret=os.environ["CLOUDINARY_API_SECRET"],
)

client = MongoClient(os.environ["MONGODB_URI"])


def get_db():
    if not hasattr(g, "mongo"):
        g.mongo = client[os.environ["MONGO_DATABASE_NAME"]]
    return g.mongo


def login_user(username, password):
    db = get_db()
    user = db.user.find_one({"username": username})
    if not user:
        return None
    if check_password(password, user["password_hash"]):
        return user
    return None


@app.route("/")
def hello_world():
    return render_template("index.html")


@app.route("/login", methods=["GET", "POST"])
def login():
    error = None
    if request.method == "POST":
        if request.form["username"] is None:
            error = "No username"
        elif request.form["password"] is None:
            error = "No password"
        else:
            logged_in = login_user(request.form["username"], request.form["password"])
            if logged_in:
                session["logged_in"] = logged_in["username"]
                # flash('You were logged in')
                next = request.form.get("next", None)
                if not next:
                    next = url_for("hello_world")
                return redirect(next)
            else:
                error = "Login incorrect"
    return render_template("login.html", error=error)


@app.route("/logout", methods=["GET", "POST"])
def logout():
    if request.method == "POST":
        session["logged_in"] = None
        return redirect(url_for("hello_world"))
    return render_template("logout.html")


@app.route("/checkin", methods=["GET", "POST"])
@login_required
def checkin():
    if not session["logged_in"]:
        return redirect(url_for("login"))
    if request.method == "POST":
        file_to_upload = request.files.get("file", None)
        if file_to_upload:
            upload_result = upload(file_to_upload)
        else:
            upload_result = None
        db = get_db()
        pvt = 1 if request.form.get("private", False) else 0
        where = {
            "where": request.form["where"],
            "neighborhood": request.form["neighborhood"],
            "locality": request.form["locality"],
            "administrative_area_level_1": request.form["administrative_area_level_1"],
            "country": request.form["country"],
            "postal_code": request.form["postal_code"],
        }
        ckin = {
            "username": session["logged_in"],
            "lat": request.form["lat"],
            "lon": request.form["lon"],
            "why": request.form["why"],
            "location": where,
            "private": pvt,
            "timestamp": datetime.now(),
        }
        if upload_result:
            ckin["image"] = upload_result
        db.checkin.insert(ckin)
    return render_template("checkin.html", mapkey=os.environ["MAPS_API_KEY"])


def _image_url(img_data):
    img_w = img_data["width"]
    img_h = img_data["height"]
    if img_w >= img_h:
        img_h = int(img_h * (400 / img_w))
        img_w = 400
    else:
        img_w = int(img_w * (300 / img_h))
        img_h = 300
    url, opt = cloudinary_url(
        img_data["public_id"], height=img_h, width=img_w, format="jpg"
    )
    return url, img_w, img_h


def _process_checkins(checkins):
    cks = []
    for c in checkins:
        if c.get("image", None):
            cks.append((c, _image_url(c["image"])))
        else:
            cks.append((c, None))
    return cks


@app.route("/my_checkins", methods=["GET"], defaults={"page": 1})
@app.route("/my_checkins/page/<int:page>", methods=["GET"])
@login_required
def my_checkins(page):
    limit = 25
    db = get_db()
    checkins = (
        db.checkin.find({"username": session["logged_in"]})
        .sort([("_id", -1)])
        .skip((page - 1) * limit)
        .limit(limit)
    )
    cks = _process_checkins(checkins)
    today = datetime.today()
    next = page - 1 if page > 1 else None
    prev = page + 1
    return render_template(
        "my_checkins.html",
        checkins=cks,
        d=today.day,
        m=today.month,
        next=next,
        prev=prev,
    )


def create_presigned_url(bucket_name, object_name, expiration=3600):
    """Generate a presigned URL to share an S3 object

    :param bucket_name: string
    :param object_name: string
    :param expiration: Time in seconds for the presigned URL to remain valid
    :return: Presigned URL as string. If error, returns None.
    """

    # Generate a presigned URL for the S3 object
    s3_client = boto3.client("s3")
    try:
        response = s3_client.generate_presigned_url(
            "get_object",
            Params={"Bucket": bucket_name, "Key": object_name},
            ExpiresIn=expiration,
        )
    except ClientError as e:
        logging.error(e)
        return None

    # The response contains the presigned URL
    return response


def process_facebook(data):
    raw = pformat(data)
    post = {}
    if data.get("data", None):
        try:
            post["post"] = data.get("data")[0].get("post")
        except:
            pass
    post["_id"] = data.get("_id")
    post["timestamp"] = datetime.fromtimestamp(data.get("timestamp"))
    if data.get("attachments", None):
        post["media"] = []
        post["place"] = []
        for item in data.get("attachments"):
            for attach in item["data"]:
                if attach.get("media"):
                    media = {}
                    media["title"] = attach.get("media").get("title")
                    media["url"] = create_presigned_url(
                        "ekbire-e6beacc4-dfdd-4add-9609-f5a60d2c5411",
                        attach.get("media").get("uri"),
                    )
                    post["media"].append(media)
                elif attach.get("place"):
                    post["place"] = attach.get("place")
    return post, raw


@app.route("/my_facebook", methods=["GET"], defaults={"page": 1})
@app.route("/my_facebook/page/<int:page>", methods=["GET"])
@login_required
def my_facebook(page):
    limit = 25
    db = get_db()
    # checkins = db.facebook.find_one({"_id": "5f84ecc11324314ada017417"})
    checkins = (
        db.facebook.find().sort([("_id", -1)]).skip((page - 1) * limit).limit(limit)
    )
    # cks = _process_checkins(checkins)
    cks = [process_facebook(c) for c in checkins]
    today = datetime.today()
    next = page - 1 if page > 1 else None
    prev = page + 1
    return render_template(
        "my_facebook.html",
        checkins=cks,
        d=today.day,
        m=today.month,
        next=next,
        prev=prev,
    )


@app.route("/today/<month>/<day>", methods=["GET"])
@login_required
def today(month, day):
    db = get_db()
    agg = [
        {
            "$project": {
                "m": {"$month": "$timestamp"},
                "d": {"$dayOfMonth": "$timestamp"},
                "u": "$username",
            }
        },
        {"$match": {"m": int(month), "d": int(day), "u": session["logged_in"]}},
        {"$sort": SON([("_id", -1),])},
    ]
    checkins = [ck["_id"] for ck in db.checkin.aggregate(agg)]
    cks = _process_checkins(db.checkin.find({"_id": {"$in": checkins}}))
    return render_template("today.html", checkins=cks)


@app.route("/view/<id>/checkin")
@login_required
def view_checkin(id):
    db = get_db()
    checkin = db.checkin.find_one(
        {"_id": ObjectId(id), "username": session["logged_in"]}
    )
    if checkin:
        checkin, iurl = _process_checkins([checkin])[0]
    return render_template("view_checkin.html", c=checkin, iurl=iurl)


@app.route("/raw/<id>/checkin")
@login_required
def view_checkin_raw(id):
    db = get_db()
    checkin = db.checkin.find_one(
        {"_id": ObjectId(id), "username": session["logged_in"]}
    )
    return render_template("view_checkin_raw.html", c=pformat(checkin, indent=3), pk=id)


@app.route("/public/<username>")
def public(username):
    db = get_db()
    checkins = (
        db.checkin.find({"username": username, "private": {"$ne": 1}})
        .sort([("_id", -1)])
        .limit(10)
    )
    cks = _process_checkins(checkins)
    return render_template("public.html", checkins=cks, user=username)


@app.route("/register", methods=["GET", "POST"])
def register():
    error = None
    if request.method == "POST":
        pwd = request.form["password"]
        pwd2 = request.form["password2"]
        if pwd != pwd2:
            error = "Passwords don't match"
        else:
            secret_key = request.form["secretkey"]
            if secret_key != os.environ["REGISTER_KEY"]:
                error = "That's not the secret key!"
            else:
                username = request.form["username"]
                db = get_db()
                if db.user.find_one({"username": username}) is not None:
                    error = "That username is already taken"
        if error is None:
            db.user.insert({"username": username, "password_hash": hash_password(pwd)})
            return render_template("register_success.html")
    return render_template("register.html", error=error)


if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0")
