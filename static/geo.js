$( document ).ready(function() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(writetopage);
  } else {
    $("#demo").html("Geolocation is not supported by this browser.");
  }
});

function writetopage(position) {
    var radius = .005;
    $("#demo").html("Latitude: " + position.coords.latitude +
    "<br>Longitude: " + position.coords.longitude);
    var la = position.coords.latitude;
    var lo = position.coords.longitude;
    checkinform.lat.value=position.coords.latitude;
    checkinform.lon.value=position.coords.longitude;
    lats = [la - radius, la + radius];
    lons = [lo - radius, lo + radius];
    var defaultBounds = new google.maps.LatLngBounds(
      new google.maps.LatLng(lats[0], lons[0]),
      new google.maps.LatLng(lats[1], lons[1])
    );

    var input = document.getElementById('where');
    var options = {
      bounds: defaultBounds,
      strictBounds: true,
      types: ['establishment']
    };

    autocomplete = new google.maps.places.Autocomplete(input, options);
    autocomplete.addListener('place_changed', fillInAddress);
  };

  var componentForm = {
          neighborhood: 'long_name',
          locality: 'long_name',
          administrative_area_level_1: 'short_name',
          country: 'long_name',
          postal_code: 'short_name'
        };

function fillInAddress(){
  var place = autocomplete.getPlace();
  for (var i = 0; i < place.address_components.length; i++) {
            var addressType = place.address_components[i].types[0];
            if (componentForm[addressType]) {
              var val = place.address_components[i][componentForm[addressType]];
              document.getElementById(addressType).value = val;
            }
          }
}

function set_place(obj){
  checkinform.place.value = $(obj).attr("data-place");
}
