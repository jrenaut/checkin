from functools import wraps
from flask import request, redirect, url_for, session

def login_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if not session.get('logged_in', None):
            return redirect(url_for('login', next=request.url))
        return f(*args, **kwargs)
    return decorated_function

def format_datetime(value, format='full'):
    if format == 'full':
        format="%H:%M %m-%d-%Y"
    elif format == 'medium':
        format="%m-%d-%Y"
    return value.strftime(format)
